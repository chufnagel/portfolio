import React from 'react';
// import {
//   Email, Git, GithubCircle, LinkedinBox, PhoneClassic,
// } from 'mdi-material-ui';
import Email from 'mdi-material-ui/Email';
import Git from 'mdi-material-ui/Git';
import GithubCircle from 'mdi-material-ui/GithubCircle';
import LinkedinBox from 'mdi-material-ui/LinkedinBox';
import PhoneClassic from 'mdi-material-ui/PhoneClassic';
import Card from '@material-ui/core/Card';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

export default function Contact() {
  return (
    <div className="Contact">
      <Card>
        <List>
          <ListItem button="true" component="a" href="https://www.linkedin.com/in/charliehufnagel" target="_blank">
            <ListItemIcon><LinkedinBox /></ListItemIcon>
            <ListItemText primary="LinkedIn" />
          </ListItem>
          <ListItem button="true" component="a" href="https://www.github.com/chufnagel" target="_blank">
            <ListItemIcon><GithubCircle /></ListItemIcon>
            <ListItemText primary="GitHub" />
          </ListItem>
          <ListItem button="true" component="a" href="https://www.gitlab.com/chufnagel" target="_blank">
            <ListItemIcon><Git /></ListItemIcon>
            <ListItemText primary="GitLab" />
          </ListItem>
          <ListItem button="true" component="a" href="mailto:charles.hufnagel@pm.me" target="_blank">
            <ListItemIcon><Email /></ListItemIcon>
            <ListItemText primary="Email" />
          </ListItem>
          <ListItem button="true" component="a" href="tel:434-987-0270" target="_blank">
            <ListItemIcon><PhoneClassic /></ListItemIcon>
            <ListItemText primary="Phone" />
          </ListItem>
        </List>
      </Card>
    </div>
  );
}
