import React, { Component } from 'react';
import { Document, Page } from 'react-pdf/dist/entry.webpack';
import Card from '@material-ui/core/Card';
import './resume.css';

class Resume extends Component {
  state = {
    file: './Assets/Charles Hufnagel Resume.pdf',
    // eslint-disable-next-line
    numPages: null,
    pageNumber: 1,
  }

  onDocumentLoadSuccess = ({ numPages }) => {
    // eslint-disable-next-line
    this.setState({ numPages });
  }

  render() {
    const { file, pageNumber } = this.state;

    return (
      <div>
        <Card>
          <Document
            file={file}
            onLoadSuccess={this.onDocumentLoadSuccess}
            renderMode="svg"
            className="resume"
          >
            <Page pageNumber={pageNumber} />
          </Document>
        </Card>
      </div>
    );
  }
}

export default Resume;
