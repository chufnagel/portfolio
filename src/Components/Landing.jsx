import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

const style = {
  height: 256,
  width: 256,
  objectFit: 'contain',
  alignItems: 'center',
};

function Landing() {
  return (
    <div>
      <Card>
        <CardMedia
          style={style}
          component="img"
          image="./Assets/portrait.jpg"
        />
        <CardContent>
          <Typography gutterBottom variant="headline">
            Charles Hufnagel
          </Typography>
          <Typography gutterBottom variant="subheading">
            Software Engineer
          </Typography>
          <Typography variant="body1">
            I am Charles Hufnagel, a full-stack
            software engineer with production-level experience working
            with servers and databases. I am fluent in JavaScript, Python, and SQL, and
            have also developed with Java and Scala.
            Areas of interest include containerization and orchestration,
            education technology, and real-time analytics.
          </Typography>
        </CardContent>
      </Card>
    </div>
  );
}

export default withStyles()(Landing);
