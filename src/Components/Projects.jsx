import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const wandererLink = <a href="https://www.charles-hufnagel.com/">Wanderer</a>;
const yinderLink = <a href="https://yelp-tinder.appspot.com/">Yinder</a>;

export default function Projects() {
  return (
    <div>
      <Card>
        <CardMedia
          component="img"
          image="./Assets/yinder-landing.png"
          title="Yinder: A Tinder-Style Restaurant Finder"
        />
        <CardContent>
          <Typography gutterBottom variant="headline" component="h1">
            {yinderLink}
          </Typography>
          <Typography gutterBottom variant="subheading">
            A Tinder-Style Restaurant Finder
          </Typography>
          <Typography component="h1">
            A Tinder-style restaurant finder.
            Built using React, Materialize-CSS, MongoDB, and
            Express. Containerized with Docker, tested with Ava,
            hosted on Google Cloud Platform. Leverages the Yelp Fusion API.
          </Typography>
        </CardContent>
      </Card>
      <Card>
        <CardMedia
          component="img"
          image="./Assets/wanderer_search_results.png"
          title="Wanderer: A Social Travel Diary"
        />
        <CardContent>
          <Typography gutterBottom variant="headline" component="h1">
            {wandererLink}
          </Typography>
          <Typography gutterBottom variant="subheading">
            A Social Travel Diary
          </Typography>
          <Typography component="h1">
            A social travel diary/experience finder.
            Created with React, Redux, Redux-Saga, Material-UI,
            and MariaDB. Site runs on AWS Elastic Container Service
            and Express with an Nginx reverse proxy and is secured
            with LetsEncrypt. End-to-end and unit test suites built
            with Enzyme, Jest, Mocha, and Selenium WebDriver.
          </Typography>
        </CardContent>
      </Card>
    </div>
  );
}
