import React from 'react';
import { Link, Route, Switch } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Briefcase, Phone, Wrench } from 'mdi-material-ui';

import Contact from './Components/Contact';
import Landing from './Components/Landing';
import Projects from './Components/Projects';
import Resume from './Components/Resume';

const LandingLink = props => <Link to="/" {...props} />;
const ContactLink = props => <Link to="/contact" {...props} />;
const ProjectsLink = props => <Link to="/projects" {...props} />;
const ResumeLink = props => <Link to="/resume" {...props} />;

const App = () => (
  <div className="App">
    <AppBar position="static">
      <Button
        component={LandingLink}
      >
        <Typography variant="title">
          Charles Hufnagel
        </Typography>
      </Button>
      <Toolbar>
        <Button
          component={ResumeLink}
        >
          <Briefcase />
          Resume
        </Button>
        <Button
          component={ProjectsLink}
        >
          <Wrench />
          Projects
        </Button>
        <Button
          component={ContactLink}
        >
          <Phone />
          Contact
        </Button>
      </Toolbar>
    </AppBar>
    <div>
      <Switch>
        <Route exact path="/" component={Landing} />
        <Route path="/resume" component={Resume} />
        <Route path="/projects" component={Projects} />
        <Route path="/contact" component={Contact} />
      </Switch>
    </div>
  </div>
);

export default App;
