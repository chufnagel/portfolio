import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

const theme = createMuiTheme({
  palette: {
    type: 'dark',
    common: {
      type: 'dark',
    },
    // primary: {
    //   main: '#2E2E37',
    //   light: '#8D8D91',
    //   dark: '#54545B',
    //   contrastText: '#70B6CC',
    // },
    // secondary: {
    //   main: '#5C95A7',
    //   light: '#70B6CC',
    //   dark: '#3E6470',
    //   contrastText: '#fff',
    // },
    // text: {
    //   primary: '#394875',
    //   secondaryTextColor: '#394875',
    // },
    background: {
      paper: '#2E2E37',
    },
  },
  typography: {
    fontSize: 16,
  },
  overrides: {
    MuiAppBar: {
      root: {
        // display: 'flex',
        // flexDirection: 'row',
      },
    },
    MuiButtonBase: {
      root: {
        paddingLeft: 10,
        paddingRight: 10,
      },
    },
    MuiCard: {
      root: {
        marginLeft: 'auto',
        marginRight: 'auto',
        alignItems: 'center',
      },
    },
    MuiCardMedia: {
      root: {
        objectFit: 'cover',
        paddingTop: 10,
        paddingBottom: 10,
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
  },
});

/* global document */
ReactDOM.render(
  <BrowserRouter>
    <MuiThemeProvider theme={theme}>
      <App />
    </MuiThemeProvider>
  </BrowserRouter>,
  document.getElementById('root'),
);
registerServiceWorker();
